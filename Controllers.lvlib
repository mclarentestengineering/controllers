﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="DAQmx" Type="Folder">
		<Item Name="DAQmx Analog Output with Feedback.lvclass" Type="LVClass" URL="../Classes/DAQmx/Classes/Analog Output/DAQmx Analog Output with Feedback.lvclass"/>
		<Item Name="DAQmx.lvclass" Type="LVClass" URL="../Classes/DAQmx/DAQmx.lvclass"/>
		<Item Name="DAQmx Digital Output.lvclass" Type="LVClass" URL="../Classes/DAQmx/Classes/Digital Output/DAQmx Digital Output.lvclass"/>
	</Item>
	<Item Name="Modbus TCP" Type="Folder">
		<Item Name="ACS355 (Modbus TCP).lvclass" Type="LVClass" URL="../Classes/TCP/Classes/ACS355 (Modbus TCP)/ACS355 (Modbus TCP).lvclass"/>
		<Item Name="TCP.lvclass" Type="LVClass" URL="../Classes/TCP/TCP.lvclass"/>
	</Item>
	<Item Name="Parent" Type="Folder">
		<Item Name="Controller.lvclass" Type="LVClass" URL="../Classes/Controller/Controller.lvclass"/>
	</Item>
	<Item Name="Serial" Type="Folder">
		<Item Name="Serial.lvclass" Type="LVClass" URL="../Classes/Serial/Serial.lvclass"/>
		<Item Name="SJ300(Serial).lvclass" Type="LVClass" URL="../Classes/Serial/Classes/SJ300(Serial)/SJ300(Serial).lvclass"/>
		<Item Name="Unico2400(Serial).lvclass" Type="LVClass" URL="../Classes/Serial/Classes/Unico2400(Serial)/Unico2400(Serial).lvclass"/>
	</Item>
	<Item Name="Tests" Type="Folder">
		<Item Name="Device" Type="Folder">
			<Item Name="ACS355 Modbus TCP.vi" Type="VI" URL="../Test Vi&apos;s/ACS355 Modbus TCP.vi"/>
			<Item Name="SJ300.vi" Type="VI" URL="../Test Vi&apos;s/SJ300.vi"/>
			<Item Name="Unico2400.vi" Type="VI" URL="../Test Vi&apos;s/Unico2400.vi"/>
		</Item>
		<Item Name="Generic" Type="Folder">
			<Item Name="Array of Classes.vi" Type="VI" URL="../Test Vi&apos;s/Array of Classes.vi"/>
			<Item Name="Test Comm Dialog.vi" Type="VI" URL="../Test Vi&apos;s/Test Comm Dialog.vi"/>
			<Item Name="Test Device Dialog.vi" Type="VI" URL="../Test Vi&apos;s/Test Device Dialog.vi"/>
		</Item>
	</Item>
	<Item Name="Documentation" Type="Folder">
		<Item Name="Controller Class Description.docx" Type="Document" URL="../Documentation/Controller Class Description.docx"/>
		<Item Name="Parent Class Descriptions.vi" Type="VI" URL="../Documentation/Parent Class Descriptions.vi"/>
		<Item Name="State Diagram Descriptions.docx" Type="Document" URL="../Documentation/State Diagram Descriptions.docx"/>
		<Item Name="State Diagram.vsd" Type="Document" URL="../Documentation/State Diagram.vsd"/>
		<Item Name="Unico2400 Drive Parameters.xlsx" Type="Document" URL="../Documentation/Unico2400 Drive Parameters.xlsx"/>
	</Item>
	<Item Name="CAN.lvclass" Type="LVClass" URL="../Classes/CAN/CAN.lvclass"/>
</Library>
