﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Controllers.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../../../Controllers.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(^!!!*Q(C=\&gt;8"&lt;2MR%!813:"$A*T51;!7JA7VI";G"6V^6!P4AFJ1#^/#7F!,TN/'-(++=IC2(-TVS+O`80+:3[QDNP9VYEO]0GP@@NM_LD_\`K4&amp;2`NI`\;^0.WE\\ZH0]8D2;2'N3K6]:DK&gt;?1D(`H)2T\SFL?]Z3VP?=N,8P+3F\TE*5^ZSF/?]J3H@$PE)1^ZS*('Z'/C-?A99(2'C@%R0--T0-0D;QT0]!T0]!S0,D%]QT-]QT-]&lt;IPB':\B':\B-&gt;1GG?W1]QS0Y;.ZGK&gt;ZGK&gt;Z4"H.UQ"NMD:Q'Q1DWM6WUDT.UTR/IXG;JXG;JXF=DO:JHO:JHO:RS\9KP7E?BZT(-&amp;%]R6-]R6-]BI\C+:\C+:\C-6U54`%52*GQ$)Y1Z;&lt;3I8QJHO,R+YKH?)KH?)L(J?U*V&lt;9S$]XDE0-E4`)E4`)EDS%C?:)H?:)H?1Q&lt;S:-]S:-]S7/K3*\E3:Y%3:/;0N*A[=&lt;5+18*YW@&lt;,&lt;E^J&gt;YEO2U2;`0'WJ3R.FOM422L=]2[[,%?:KS(&amp;'PR9SVKL-7+N1CR`LB9[&amp;C97*0%OPH2-?Y_&lt;_KK,OKM4OKI$GKP&gt;I^&lt;`X,(_`U?N^MNLN&gt;L8#[8/*`0=4K&gt;YHA]RO&amp;QC0V_(\P&gt;\OUV].XR^E,Y_6Z[=@YH^5\`3`_$&gt;W.]DF`(N59`!/&lt;!-PQ!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"(65F.31QU+!!.-6E.$4%*76Q!!$:1!!!2"!!!!)!!!$81!!!!T!!!!!B&amp;$&lt;WZU=G^M&lt;'6S=SZM&gt;GRJ9BR%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!!!+!8!)!!!$!!!#A!!!!!!!!%!!-!0!#]!"^!!!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!C"`I+&lt;G-057H.SFN!NC52A!!!!Q!!!!1!!!!!-DB!E/*'("!H\F`I.G-S&amp;T5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!"6&gt;]*&gt;B&gt;K?1+,4M,)=\9%,!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%/&gt;D#.#,@KEB8.&lt;;+N&amp;E8'E!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!C!!!!((C=9W"D9'JAO-!!R)R!T.4!^!0)`A$C-Q!!;!%).A!!!!!!1Q!!!2BYH'.AQ!4`A1")-4)QM)#Y,'DC9"L'JC&lt;!:3YOO[$CT&amp;!XME*%'9&amp;CT#_!$#;1(+J?&amp;A9E\]!"'R;T!96Q*@!!!!!!$!!"6EF%5Q!!!!!!!Q!!!59!!!,]?*Q,9'2A++]Q-XE!J*G"7*SBA3%Z0S76CQ():U#!'UQ-&amp;)%'K(F;;/+'"Q[H!9%?PXQ,G/`2`%&lt;$M`E(5[GARQ[9GP]80*K0;"TW['Y%#2VX3!!L\'9UP/2K?/$`B!NA&lt;5!&amp;`0)/90U.F&gt;9-*?S'"]##RRO`-%+-Q4!1J(Y(30)QKPU?H2=]$DZA]93L^UI79@"B0/$2[=*F=.S&amp;+U!-J*$R&amp;"9$'=+!"CBY&gt;*Z$S/VCB#DI:P09#20T[01)1/&amp;J)0-[Q$Z#&amp;PC!,P!!R4^!SR8C$G-.^\7P\_U#B4M&lt;EJA$!S1_,D#C9DU'2A;19U&amp;E,F3N$:$.""74A9K"W0OA&lt;!UE0&lt;?1T)?*T17LA&gt;D("B6\"_7$W(_!&gt;!#5T185.!(+FA;S";"M(:$\I'R&lt;+0M#.&amp;*RU=\_,KYQ.D*!4M=!U[^UB!!!!!!!$"=!A"%!!!1R.SYQ!!!!!!Q8!)!!!!!%-4=O-!!!!!!-&amp;Q#!%1!!"$%X,D!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Q8!)!2!!!%-4=O-!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!9!!"A:A!!99'!!'9!9!"I!"!!&lt;!!Q!'M!U!"IQ\!!;$V1!'A+M!"I$6!!;!KQ!'A.5!"I#L!!;!V1!'9+Y!"BD9!!9'Y!!'!9!!"`````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!,#Q!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!,L6_*L1M!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!,L6]V.45VC;U,!!!!!!!!!!!!!!!!!!!!!0``!!!,L6]V.45V.45V.9GN#Q!!!!!!!!!!!!!!!!!!``]!C6]V.45V.45V.45V.47*L1!!!!!!!!!!!!!!!!$``Q"@8T5V.45V.45V.45V.@[*!!!!!!!!!!!!!!!!!0``!&amp;_*C6]V.45V.45V.@\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9F@.45V.@\_`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C9G*8[X_`P\_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*C9G*`P\_`P\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9G*C9H_`P\_`P\_8Q!!!!!!!!!!!!!!!!$``Q"@C9G*C9G*C@\_`P\_`PZ@!!!!!!!!!!!!!!!!!0``!&amp;_*C9G*C9G*`P\_`P\_`F]!!!!!!!!!!!!!!!!!``]!8YG*C9G*C9H_`P\_`P\_8Q!!!!!!!!!!!!!!!!$``Q#*C9G*C9G*C@\_`P\_`IG*!!!!!!!!!!!!!!!!!0``!!"@8YG*C9G*`P\_`IGN8Q!!!!!!!!!!!!!!!!!!``]!!!!!8YG*C9H_`IG*8Q!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!&amp;_*C9G*.1!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!"@.1!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!!A!"!!!!!!!-!!&amp;'5%B1!!!!!!!$!!!#;1!!"/*YH+W5TWM4124(XY26*K(&amp;32KV#QW*:2KL*"&gt;`VG#UN..$J21F&amp;DQ5&lt;($D$WC..'H6A_VF%8)I&amp;)1=B"[]Z/IB"_]3P/R"4RYMO#2`A:?C9$@JWUFW.YE9,S9Q$-H\P/`&lt;^`WS!-JX.OJLQJ9&amp;B"XA:='#A'91A&amp;K31O=4@QVMC@Q'%F;*"&gt;.UC?X\GG4-AC(.C..,IA1`/N5E4&gt;[R/J9?:SIW#VBQ1D./"W^RE`(X9\RUT/GKQDGW1ZK_?TT[E_\I?21%@&gt;)_AUH3"#,/+)I_@DP\.+&gt;T_V&gt;`EKKSJ&gt;]#*ITB47[?R9YI`6'W*,MEA&gt;+&gt;FI!N*[&amp;;L8J1M!X&amp;Z2B4S*#%((&gt;X!".#ZDEX%Z)*3!:VUI[/C$&lt;@W,0&lt;5$][QIU#.R&amp;\Y7QGY5C*K$5NM5;DA2C?(?SR"7&amp;OTF#6VL6P:0C/`A%)E.J^VY@AIGW#L!WB$R-JQD*YTVBQ8D&gt;]+URRNPM+[N)&amp;R8(BOOX#D(4",UI-0"PK`\3B.B[:8&gt;UI&amp;(0LM@T$W)06&lt;+%1?\&lt;_:$.&lt;T-7U&lt;$(\JU&amp;J9;A8\;?XR&gt;L:C)!#,W'F?^FZK&amp;1KO!!]0@1'IC0=&gt;$F60IXG,FTT&amp;G[L?JO\C:M4P_Y_ML=XE@)ZE15PL"=QL-&amp;Q`U!^9&lt;X]`].[22C2L&lt;[QQD*_VQ9%\WI&lt;]M++^1"LM$S!G5*GOTOM&lt;26(:V"9LQG$^;&lt;6:PP37C[8?TD=?=J.KU*)/[X[F^:B[\!`M_"@\*K=\H`;=0_H'3Z;60V\!:N$M7U,4IL:U'=WD^W'[$S&gt;9XPI+L[.4N%^?F$\[J&lt;8&amp;NQL@ROFIU&gt;'XV?3!!!!!!!!"!!!!#!!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!"C!!!!=HC=9W"AS"/190L(50?8A5HA+Z!B`:?"7&gt;#0]4=$![?@Q'%AT3AA#237`=P!,KA.&amp;N9_IMP"!!7K&lt;)Q=EBS("4H!-BQN'AT````H_(LE'FT&amp;%2]Y5W770)=%!"2C'1!!!!!!!!1!!!!(!!!!P!!!!!)!!!!B8WZJ8URB=X2,&lt;G^X&lt;E^X&lt;GFO:UR71WRB=X.$&lt;(6T&gt;'6S!!!!3"=!A!!!!!!"!!A!-0````]!!1!!!!!!,!!!!!%!*%"1!!!=2%&amp;2&lt;8AA2'FH;82B&lt;#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!!1!!!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;597*0=G2F=A!!!#58!)!!!!!!!A!&amp;!!=!!!Q!1!!"`````Q!!!!%!!1!!!!!!!!!!!!!!"!!#!!A!!!!%!!!!1!!!!#A!!!!#!!!%!!!!!!%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!]A!!!8NYH)V0SUY#12#M::#H)OC&amp;AS:T]/$*CT_QB!V8U3^Q:(@**AV$&gt;HP*(PUN@]7`Y!_I"9Q(0:"+/N.6X684!'Z29@@NPBI!T'DKVZJ\E31PHG1LW=&gt;&gt;.(F&gt;64&lt;+FJE[M3_F&lt;EKFN""8&amp;/.`R95+(M-Z=$_6MN!ENT[VBXG\S&lt;/NU]4'4BUO#011-X?-A)UZN8DGMROW96*:IB._'P_G2S,R+9&lt;U.BRPI7X+/%7PTD*;R&gt;S?IY%/OHAY+Z]XVT9M!&lt;DWTOAG:D3RO-16S2I$BPUA//%P][N=(X!]*-#1DO#(Q,!GW4Z'O'(N\Q(!MU*W!!!!!!"F!!%!!A!$!!1!!!")!!]%!!!!!!]!W!$6!!!!51!0"!!!!!!0!.A!V1!!!&amp;I!$Q1!!!!!$Q$9!.5!!!"D!"C%!!0I!"="7!%`#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!!!!"35V*$$1I!!UR71U.-1F:8!!!.F!!!"%%!!!!A!!!.&gt;!!!!!!!!!!!!!!!)!!!!$1!!!1I!!!!(%R*1EY!!!!!!!!"9%R75V)!!!!!!!!"&gt;&amp;*55U=!!!!!!!!"C%.$5V1!!!!!!!!"H%R*&gt;GE!!!!!!!!"M%.04F!!!!!!!!!"R&amp;2./$!!!!!!!!!"W%2'2&amp;-!!!!!!!!"\%R*:(-!!!!!!!!#!&amp;:*1U1!!!!!!!!#&amp;(:F=H-!!!!%!!!#+&amp;.$5V)!!!!!!!!#D%&gt;$5&amp;)!!!!!!!!#I%F$4UY!!!!!!!!#N'FD&lt;$A!!!!!!!!#S%.11T)!!!!!!!!#X%R*:H!!!!!!!!!#]%:13')!!!!!!!!$"%:15U5!!!!!!!!$'&amp;:12&amp;!!!!!!!!!$,%R*9G1!!!!!!!!$1%*%3')!!!!!!!!$6%*%5U5!!!!!!!!$;&amp;:*6&amp;-!!!!!!!!$@%253&amp;!!!!!!!!!$E%V6351!!!!!!!!$J%B*5V1!!!!!!!!$O&amp;:$6&amp;!!!!!!!!!$T%:515)!!!!!!!!$Y!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/!!!!!!!!!!!`````Q!!!!!!!!$=!!!!!!!!!!$`````!!!!!!!!!0!!!!!!!!!!!0````]!!!!!!!!!_!!!!!!!!!!!`````Q!!!!!!!!%E!!!!!!!!!!$`````!!!!!!!!!3Q!!!!!!!!!!0````]!!!!!!!!"6!!!!!!!!!!!`````Q!!!!!!!!'=!!!!!!!!!!$`````!!!!!!!!!;Q!!!!!!!!!"0````]!!!!!!!!#_!!!!!!!!!!(`````Q!!!!!!!!-)!!!!!!!!!!D`````!!!!!!!!!RA!!!!!!!!!#@````]!!!!!!!!$+!!!!!!!!!!+`````Q!!!!!!!!-Y!!!!!!!!!!$`````!!!!!!!!!UA!!!!!!!!!!0````]!!!!!!!!$9!!!!!!!!!!!`````Q!!!!!!!!.U!!!!!!!!!!$`````!!!!!!!!!`A!!!!!!!!!!0````]!!!!!!!!(`!!!!!!!!!!!`````Q!!!!!!!!A%!!!!!!!!!!$`````!!!!!!!!#"1!!!!!!!!!!0````]!!!!!!!!+B!!!!!!!!!!!`````Q!!!!!!!!K-!!!!!!!!!!$`````!!!!!!!!#J1!!!!!!!!!!0````]!!!!!!!!+J!!!!!!!!!!!`````Q!!!!!!!!M-!!!!!!!!!!$`````!!!!!!!!#R1!!!!!!!!!!0````]!!!!!!!!,V!!!!!!!!!!!`````Q!!!!!!!!P=!!!!!!!!!!$`````!!!!!!!!#_1!!!!!!!!!!0````]!!!!!!!!-%!!!!!!!!!#!`````Q!!!!!!!!U)!!!!!"B%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#ZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>
<Name></Name>
<Val>!!!!!B&amp;$&lt;WZU=G^M&lt;'6S=SZM&gt;GRJ9BR%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#ZM&gt;G.M98.T!&amp;"53$!!!!!!!!!!!!!!!!)!!1!!!!!!!!!!!!!"!#2!5!!!(%2"57VY)%2J:WFU97QA4X6U=(6U,GRW9WRB=X-!!!%!!!!!!!!!!!!!!!!"$ERB9F:*26=A4W*K:7.U!&amp;"53$!!!!!!!!!!!!!8!)!!!!!!!!!!!P``!!!!!1!!!!!!!1!!!!!"!#2!5!!!(%2"57VY)%2J:WFU97QA4X6U=(6U,GRW9WRB=X-!!!%!!!!!!!(````_!!!!!!!!!B&amp;$&lt;WZU=G^M&lt;'6S=SZM&gt;GRJ9AV%16&amp;N?#ZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!"=!A!!!!!!!!!!!!!!!</Val>
</String>
</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!""!!!!!B&amp;$&lt;WZU=G^M&lt;'6S=SZM&gt;GRJ9AV%16&amp;N?#ZM&gt;G.M98.T5&amp;2)-!!!!"5!!1!%!!!!$52"57VY,GRW9WRB=X-!!!!!</Property>
	<Item Name="DAQmx Digital Output.ctl" Type="Class Private Data" URL="DAQmx Digital Output.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Methods" Type="Folder">
		<Item Name="Overriden" Type="Folder">
			<Item Name="Device Parameters.vi" Type="VI" URL="../Methods/Overriden/Device Parameters.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'H!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;2!=!!?!!!Q%5.P&lt;H2S&lt;WRM:8*T,GRW&lt;'FC(%2"57VY)%2J:WFU97QA4X6U=(6U,GRW9WRB=X-!!"B%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!'%!Q`````QZ4&gt;(*J&lt;G=A1W^O&gt;(*P&lt;!!!,%"!!!(`````!!=?1WBB&lt;GZF&lt;(-A5'^T=WFC&lt;'5A:G^S)%:F:72C97.L!!"31(!!(A!!-"&amp;$&lt;WZU=G^M&lt;'6S=SZM&gt;GRJ9BR%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!82%&amp;2&lt;8AA2'FH;82B&lt;#"0&gt;82Q&gt;81A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!)!!E$!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!##!!!!*)!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Get Channel Names.vi" Type="VI" URL="../Methods/Overriden/Get Channel Names.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'2!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!-0````]'5X2S;7ZH!!!?1%!!!@````]!"2"$&lt;WZU=G^M&lt;'6S)%ZB&lt;76T!!"51(!!(A!!-"&amp;$&lt;WZU=G^M&lt;'6S=SZM&gt;GRJ9BR%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#ZM&gt;G.M98.T!!!92%&amp;2&lt;8AA2'FH;82B&lt;#"0&gt;82Q&gt;81A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;*!=!!?!!!Q%5.P&lt;H2S&lt;WRM:8*T,GRW&lt;'FC(%2"57VY)%2J:WFU97QA4X6U=(6U,GRW9WRB=X-!!"&gt;%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#"J&lt;A"5!0!!$!!$!!1!"A!(!!1!"!!%!!1!#!!%!!1!#1-!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!!!!!!EA!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Initialize References.vi" Type="VI" URL="../Methods/Overriden/Initialize References.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;D!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;2!=!!?!!!Q%5.P&lt;H2S&lt;WRM:8*T,GRW&lt;'FC(%2"57VY)%2J:WFU97QA4X6U=(6U,GRW9WRB=X-!!"B%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!5E"Q!"Y!!$!21W^O&gt;(*P&lt;'RF=H-O&lt;(:M;7)=2%&amp;2&lt;8AA2'FH;82B&lt;#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!&amp;U2"57VY)%2J:WFU97QA4X6U=(6U)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1351361040</Property>
			</Item>
			<Item Name="Initialize.vi" Type="VI" URL="../Methods/Overriden/Initialize.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;D!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;2!=!!?!!!Q%5.P&lt;H2S&lt;WRM:8*T,GRW&lt;'FC(%2"57VY)%2J:WFU97QA4X6U=(6U,GRW9WRB=X-!!"B%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!5E"Q!"Y!!$!21W^O&gt;(*P&lt;'RF=H-O&lt;(:M;7)=2%&amp;2&lt;8AA2'FH;82B&lt;#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!&amp;U2"57VY)%2J:WFU97QA4X6U=(6U)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1351361040</Property>
			</Item>
			<Item Name="Send Setpoint.vi" Type="VI" URL="../Methods/Overriden/Send Setpoint.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'V!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;2!=!!?!!!Q%5.P&lt;H2S&lt;WRM:8*T,GRW&lt;'FC(%2"57VY)%2J:WFU97QA4X6U=(6U,GRW9WRB=X-!!"B%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!/%"Q!"Y!!#-.2W6O:8*B&lt;#ZM&gt;GRJ9B.1=G^H=G&amp;N)%RP:SZM&gt;G.M98.T!!N1=G^H=G&amp;N)%RP:Q!;1#%66W&amp;J&gt;#"P&lt;C"/:8BU)&amp;.F&gt;("P;7ZU!&amp;*!=!!?!!!Q%5.P&lt;H2S&lt;WRM:8*T,GRW&lt;'FC(%2"57VY)%2J:WFU97QA4X6U=(6U,GRW9WRB=X-!!"&gt;%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!(!!A!#1-!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!A!!!!)!!!!EA!!!!!"!!I!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Soft Stop Parameters.vi" Type="VI" URL="../Methods/Overriden/Soft Stop Parameters.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;D!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;2!=!!?!!!Q%5.P&lt;H2S&lt;WRM:8*T,GRW&lt;'FC(%2"57VY)%2J:WFU97QA4X6U=(6U,GRW9WRB=X-!!"B%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!5E"Q!"Y!!$!21W^O&gt;(*P&lt;'RF=H-O&lt;(:M;7)=2%&amp;2&lt;8AA2'FH;82B&lt;#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!&amp;U2"57VY)%2J:WFU97QA4X6U=(6U)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Soft Stop.vi" Type="VI" URL="../Methods/Overriden/Soft Stop.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;D!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;2!=!!?!!!Q%5.P&lt;H2S&lt;WRM:8*T,GRW&lt;'FC(%2"57VY)%2J:WFU97QA4X6U=(6U,GRW9WRB=X-!!"B%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!5E"Q!"Y!!$!21W^O&gt;(*P&lt;'RF=H-O&lt;(:M;7)=2%&amp;2&lt;8AA2'FH;82B&lt;#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!&amp;U2"57VY)%2J:WFU97QA4X6U=(6U)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
			<Item Name="Start.vi" Type="VI" URL="../Methods/Overriden/Start.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;D!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!&amp;2!=!!?!!!Q%5.P&lt;H2S&lt;WRM:8*T,GRW&lt;'FC(%2"57VY)%2J:WFU97QA4X6U=(6U,GRW9WRB=X-!!"B%16&amp;N?#"%;7&gt;J&gt;'&amp;M)%^V&gt;("V&gt;#"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!5E"Q!"Y!!$!21W^O&gt;(*P&lt;'RF=H-O&lt;(:M;7)=2%&amp;2&lt;8AA2'FH;82B&lt;#"0&gt;82Q&gt;81O&lt;(:D&lt;'&amp;T=Q!!&amp;U2"57VY)%2J:WFU97QA4X6U=(6U)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#3!!!!!!%!#!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
		</Item>
		<Item Name="Private" Type="Folder"/>
	</Item>
	<Item Name="Type Defs" Type="Folder"/>
</LVClass>
